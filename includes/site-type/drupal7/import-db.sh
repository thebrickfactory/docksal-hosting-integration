#!/bin/bash

## Drupal 7 import-db.sh script.

## This process is exactly the same in Drupal 7 as it is in Drupal 8, so we
## just use that code.

source "${PROJECT_ROOT}/.docksal/includes/site-type/drupal8/import-db.sh"
