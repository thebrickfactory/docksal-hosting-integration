#!/bin/bash

## Drupal 7 local.sh script.

## In Drupal 7, reverts config and clears the drupal cache.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

progressMessage "Run drush updatedb..." && drush -y updatedb
progressMessage "Run drush -y fra..." && drush -y fra
