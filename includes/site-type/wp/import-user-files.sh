#!/bin/bash

## Worpress import-user-files.sh script.

## This is meant to define the path where the user files are located from the
## document root. Define that path with the variable "USER_FILES_PATH".

USER_FILES_PATH="wp-content/uploads"
