#!/bin/bash

## Wordpress init.sh script.

## Define all of the path options.
PATH_FINAL_SETTINGS="${PROJECT_ROOT}/${DOCROOT}/wp-config-local.php"
PATH_DEFAULT_SETTINGS="${PROJECT_ROOT}/.docksal/settings/wp/wp-config-local.php"
PATH_OVERRIDES_SETTINGS="${PROJECT_ROOT}/.docksal/overrides/settings/wp-config-local.php"
PATH_MAIN_SETTINGS="${PROJECT_ROOT}/${DOCROOT}/wp-config.php"

## If the config override files exist, use that instead of the default.
if [ -f "${PATH_OVERRIDES_SETTINGS}" ]; then
  echo "Found WP override config file."
  PATH_DEFAULT_SETTINGS="${PATH_OVERRIDES_SETTINGS}"
fi

## If there is not a wp-config.php file, make the local one that file.
if [ ! -f "${PATH_MAIN_SETTINGS}" ]; then
  PATH_FINAL_SETTINGS="${PATH_MAIN_SETTINGS}"
fi

## Copy the config file if it isn't already there.
if [ ! -f "${PATH_FINAL_SETTINGS}" ]; then
  # Create the directory if it doesn't exist (which is the case before platform build is run).
  mkdir -p "${PROJECT_ROOT}/${DOCROOT}"

  # Copy the wp-config-local.php file.
  sudo cp "${PATH_DEFAULT_SETTINGS}" "${PATH_FINAL_SETTINGS}"
  sudo chmod 0777 "${PATH_FINAL_SETTINGS}"
  
  echo "Copied '${PATH_DEFAULT_SETTINGS}' to '${PATH_FINAL_SETTINGS}'."
else
  echo "wp-config-local.php already exists."
fi

## Define all of the path options.
HTACCESS_FINAL="${PROJECT_ROOT}/${DOCROOT}/.htaccess"
HTACCESS_DEFAULT="${PROJECT_ROOT}/.docksal/settings/wp/wp-local.htaccess"
HTACCESS_OVERRIDE="${PROJECT_ROOT}/.docksal/overrides/settings/wp-local.htaccess"

## If the override files exist, use that instead of the default.
if [ -f "${HTACCESS_OVERRIDE}" ]; then
  echo "Found WP htaccess override file."
  HTACCESS_DEFAULT="${HTACCESS_OVERRIDE}"
fi

## Copy the htaccess file if it isn't already there.
if [ ! -f "${HTACCESS_FINAL}" ]; then
  sudo cp "${HTACCESS_DEFAULT}" "${HTACCESS_FINAL}"
  echo "Copied '${HTACCESS_DEFAULT}' to '${HTACCESS_FINAL}'."
else
  echo "WP htaccess file already exists."
fi

