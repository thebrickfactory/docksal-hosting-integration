#!/bin/bash

## Drupal 8 local.sh script.

## In Drupal 8, reverts config and clears the drupal cache.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

progressMessage "Run drush updatedb..." && drush -y updatedb
progressMessage "Run drush config-import..." && drush -y config-import
progressMessage "Run drush cr..." && drush -y cr

# Since Drupal 8.7.0, this is no longer supported.
# See: https://www.drupal.org/project/drupal/releases/8.7.0
#progressMessage "Run drush entup..." && drush -y entup
