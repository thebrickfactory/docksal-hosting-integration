#!/bin/bash

######
# Color Variables
######
DEFAULT_COLOR="\033[0;0m"
GREEN="\033[0;32m"
RED="\033[0;31m"
BOLD_WHITE="\033[1;37m"
BOLD_GREEN="\033[1;32m"
BOLD_READ="\033[1;31m"


######
# Helper Variables
######

# The paths to the .docksal folder in the project.
PATH_DOCKSAL_HOST="${PROJECT_ROOT}/.docksal"
PATH_DOCKSAL_CLI="/var/www/.docksal"

# Define all the host type paths.
PATH_HOST_TYPE_INIT="includes/host-type/${INTEGRATION_HOST_TYPE}/init.sh"
PATH_HOST_TYPE_IMPORT_DB="includes/host-type/${INTEGRATION_HOST_TYPE}/import-db.sh"
PATH_HOST_TYPE_IMPORT_USER_FILES="includes/host-type/${INTEGRATION_HOST_TYPE}/import-user-files.sh"
PATH_HOST_TYPE_DEPLOY="includes/host-type/${INTEGRATION_HOST_TYPE}/deploy.sh"

# Define all the site type paths.
PATH_SITE_TYPE_INIT="includes/site-type/${INTEGRATION_SITE_TYPE}/init.sh"
PATH_SITE_TYPE_IMPORT_DB="includes/site-type/${INTEGRATION_SITE_TYPE}/import-db.sh"
PATH_SITE_TYPE_IMPORT_USER_FILES="includes/site-type/${INTEGRATION_SITE_TYPE}/import-user-files.sh"
PATH_SITE_TYPE_LOCAL="includes/site-type/${INTEGRATION_SITE_TYPE}/local.sh"

# Define all override paths.
PATH_OVERRIDE_TYPE_INIT="overrides/commands/init.sh"
PATH_OVERRIDE_TYPE_IMPORT_DB="overrides/commands/import-db.sh"
PATH_OVERRIDE_TYPE_IMPORT_USER_FILES="overrides/commands/import-user-files.sh"
PATH_OVERRIDE_TYPE_LOCAL="overrides/commands/local.sh"
PATH_OVERRIDE_TYPE_DEPLOY="overrides/commands/deploy.sh"

# Database variables.
PATH_DB_BASE_HOST="${PROJECT_ROOT}/db"
PATH_DB_BASE_CLI="/var/www/db"


######
# Functions
######

# Display a starting message.
function startMessage {
  printf "${BOLD_GREEN}\n-------------------------------------------------------------------\n| %s\n-------------------------------------------------------------------${DEFAULT_COLOR}\n" "${1}"
}

# Display a progress message.
function progressMessage {
  printf "\n${BOLD_WHITE}%s${DEFAULT_COLOR}\n" "${1}"
}

# Display a success message.
function successMessage {
  printf "\n${GREEN}%s${DEFAULT_COLOR}\n" "${1}"
}

# Display an error message.
function faileMessage {
  printf "\n${BOLD_READ}%s${DEFAULT_COLOR}\n\n" "${1}"
  exit
}
