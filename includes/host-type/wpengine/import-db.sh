#!/bin/bash

## WPEngine import-db.sh script.

## Downloads the database dump from a WPEngine environment.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"
WPENGINE_HOST="${INTEGRATION_SITE_ID}@${INTEGRATION_SITE_ID}.ssh.wpengine.net"
DB_FILENAME="${2}.sql"
DB_PATH_FILENAME="${PATH_DB_BASE_CLI}/${DB_FILENAME}"
DB_WPENGINE_DIR="/home/wpe-user/sites/${INTEGRATION_SITE_ID}"
DB_WPENGINE_FILE="${DB_WPENGINE_DIR}/${DB_FILENAME}"

progressMessage "Creating the WPEngine DB backup file..."

# SSH into WPEngine and export the db.
ssh ${WPENGINE_HOST} -t "cd ${DB_WPENGINE_DIR} && wp db export ${DB_FILENAME}"

progressMessage "Downloading the WPEngine DB backup file..."

# Download the DB file.
rsync -avz ${WPENGINE_HOST}:${DB_WPENGINE_FILE} ${PATH_DB_BASE_CLI}

progressMessage "Scrubbing the WPEngine DB backup file..."

# Replace url's.
sed -i "s/${INTEGRATION_SITE_ID}.wpengine.com/${VIRTUAL_HOST}/g" ${DB_PATH_FILENAME}
