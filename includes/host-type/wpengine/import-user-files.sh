#!/bin/bash

## WPEngine import-user-files.sh script.

## Downloads the user files from a WPEngine environment.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
WPENGINE_HOST="${INTEGRATION_SITE_ID}@${INTEGRATION_SITE_ID}.ssh.wpengine.net"
USER_FILES_PATH="wp-content"
WPENGINE_USER_FILES_PATH="/home/wpe-user/sites/${INTEGRATION_SITE_ID}/${USER_FILES_PATH}/uploads"
LOCAL_USER_FILES_PATH="${PROJECT_ROOT}/${DOCROOT}/${USER_FILES_PATH}"

progressMessage "Downloading the user files from WPEngine..."

# Download the DB file.
# This rsync command displays progress and only copies images (PNG and JPG).
rsync -rLvz --size-only --info=progress2 --info=name0 --include="*/" --include="*.[Jj][Pp][Gg]" --include="*.[Pp][Nn][Gg]" ${WPENGINE_HOST}:${WPENGINE_USER_FILES_PATH} ${LOCAL_USER_FILES_PATH}

# This command produced the error "bash: /usr/bin/rsync: Argument list too long"
# New command should fix this issue.
#rsync -avz ${WPENGINE_HOST}:${WPENGINE_USER_FILES_PATH}/* ${LOCAL_USER_FILES_PATH}
