#!/bin/bash

## Platform import-db.sh script.

## Downloads the database dump from a Platform.sh environment.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"
DB_FILENAME="${PATH_DB_BASE_CLI}/${2}.sql.gz"
DATABASE="${INTEGRATION_DATABASE}"

if [ "${DATABASE}" == "default" ] ; then
  # If database is set to default, use the default platform database.
  DATABASE="database"
fi

# Download the latest db backup from the platform environment, if cached file
# doesn't exist.
if [ ! -f ${DB_FILENAME} ] ; then
  platform db:dump --gzip --project="${INTEGRATION_SITE_ID}" --environment="${ENVIRONMENT}" --relationship="${DATABASE}" --file="${DB_FILENAME}" -y
  echo "Exported database."
fi
