#!/bin/bash

## Pantheon import-user-files.sh script.

## Downloads the latest user files backup from a Pantheon environment.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"
USER_FILES_PATH="$PROJECT_ROOT/${DOCROOT}/${2}"

progressMessage "Downloading the user files from Pantheon..."

# rsync the user files.
rsync -rLvz --size-only --ipv4 --progress -e 'ssh -p 2222' $ENVIRONMENT.$INTEGRATION_SITE_ID@appserver.$ENVIRONMENT.$INTEGRATION_SITE_ID.drush.in:files/ ${USER_FILES_PATH}

# Update file permsions.
sudo chmod -R 777 "${USER_FILES_PATH}"
