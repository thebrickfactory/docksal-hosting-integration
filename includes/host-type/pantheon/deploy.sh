#!/bin/bash

## Pantheon deploy.sh script.

## Pushes code to a Pantheon environment.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"

progressMessage "Starting push to the site id '${INTEGRATION_SITE_ID}' on the Pantheon environment '${ENVIRONMENT}'."

# Add pantheon repo as remote in git if this isn't there.
if ! git config remote.pantheon-${ENVIRONMENT}.url > /dev/null ; then
  git remote add pantheon-${ENVIRONMENT} ssh://codeserver.${ENVIRONMENT}.${INTEGRATION_SITE_ID}@codeserver.${ENVIRONMENT}.${INTEGRATION_SITE_ID}.drush.in:2222/~/repository.git
fi

# This is dangerous, but we are assuming this repo contains all the changes we
# need.
git push pantheon-${ENVIRONMENT} --force

# Run commands on the pantheon environment.
if [ "${INTEGRATION_SITE_NAME}" != '' ] ; then
  progressMessage "Update terminus aliases..." && terminus aliases
  if [ "${INTEGRATION_SITE_TYPE}" = 'drupal8' ] ; then
    progressMessage "Run drush updatedb..." && drush @pantheon.${INTEGRATION_SITE_NAME}.${ENVIRONMENT} -y updatedb
    progressMessage "Run drush -y cim..." && drush @pantheon.${INTEGRATION_SITE_NAME}.${ENVIRONMENT} -y cim
    progressMessage "Run drush cr..." && drush @pantheon.${INTEGRATION_SITE_NAME}.${ENVIRONMENT} cr
  elif [ "${INTEGRATION_SITE_TYPE}" == 'drupal7' ] ; then
    progressMessage "Run drush updatedb..." && drush @pantheon.${INTEGRATION_SITE_NAME}.${ENVIRONMENT} -y updatedb
    progressMessage "Run drush -y fra..." && drush @pantheon.${INTEGRATION_SITE_NAME}.${ENVIRONMENT} -y fra
    progressMessage "Run drush cc all..." && drush @pantheon.${INTEGRATION_SITE_NAME}.${ENVIRONMENT} cc all
  fi
fi

progressMessage "Pantheon url:"
terminus env:view ${INTEGRATION_SITE_ID}.${ENVIRONMENT} --print

successMessage "Finish deployment to pantheon environment!"
