#!/bin/bash

## Other import-db.sh script.

## We are not connected to a hosting provider to grab the database
## automatically. Instead manually download the db dump and place that file in
## db/db.sql or db/db.sql.gz. We don't do anything in this file right now.
