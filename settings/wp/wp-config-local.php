<?php
define('DB_NAME', 'default');
define('DB_USER', 'user');
define('DB_PASSWORD', 'user');
define('DB_HOST', 'db');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
$table_prefix = 'wp_';

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '');
define('SECURE_AUTH_KEY',  '');
define('LOGGED_IN_KEY',    '');
define('NONCE_KEY',        '');
define('AUTH_SALT',        '');
define('SECURE_AUTH_SALT', '');
define('LOGGED_IN_SALT',   '');
define('NONCE_SALT',       '');

// WP Debug Mode
define('WP_DEBUG', false);

// Define temporary directory location.
define('WP_TEMP_DIR', '/tmp');

// Require the site loads over https if config value set.
if ($_SERVER['INTEGRATION_FORCE_HTTPS'] === 'true') {
  if (!isset($_SERVER['HTTPS']) && php_sapi_name() != "cli" && $_SERVER["HTTP_X_FORWARDED_PROTO"] != 'https') {
    header('HTTP/1.0 301 Moved Permanently');
    header('Location: https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    exit();
  }
}

// In docksal, set HTTPS on to get the site to load with SSL.
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
  $_SERVER['HTTPS'] = 'on';
}

# That's It. Pencils down
if ( !defined('ABSPATH') )
  define('ABSPATH', __DIR__ . '/');
  
# Only include this file if there isn't a wp-config.php file in the repo!
require_once(ABSPATH . 'wp-settings.php');
