<?php
/**
 * @file
 * Default drupal settings file to work with docksal.
 */

// Add your database configuration here (and uncomment the block).
$databases['default']['default'] = array(
  'driver' => 'mysql',
  'host' => 'db',
  'username' => 'user',
  'password' => 'user',
  'database' => 'default',
  'prefix' => '',
);

$conf['cache'] = 0;                       // Page cache
$conf['page_cache_maximum_age'] =  0;     // External cache TTL
$conf['preprocess_css'] = FALSE;          // Optimize css
$conf['preprocess_js'] = FALSE;           // Optimize javascript
$conf['views_skip_cache'] = TRUE;         // Views caching

// Use the docksal VIRTUAL_HOST value.
$base_url = 'http://' . getenv('VIRTUAL_HOST');

// Require the site loads over https if config value set.
if ($_SERVER['INTEGRATION_FORCE_HTTPS'] === 'true') {
  $base_url = 'https://' . getenv('VIRTUAL_HOST');
  if (!isset($_SERVER['HTTPS']) && php_sapi_name() != "cli" && $_SERVER["HTTP_X_FORWARDED_PROTO"] != 'https') {
    header('HTTP/1.0 301 Moved Permanently');
    header('Location: https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    exit();
  }
}

// SOLR
// To get solr working, do the following:
// - Uncommented the "services:" and the solr lines in the 
//   .docksal/docksal-local.yml file. Run `fin p restart`.
// - Install these modules: search_api, search_api_solr, search_api_page, and search_api_override modules.
// - Configure the solr configuration to work with your host on their dev/live
//   environment. Export this to config.
// - Update the settings below by uncommenting the below code and replacing 
//   "main_solr_server" with the machine name of the solr server you configured.
// - If you go to the search api config page, it should say you can connect to
//   the solr server.

//$conf['search_api_override_mode'] = 'load';
//$conf['search_api_override_servers'] = array(
//  'main_solr_server' => array(
//    'name' => 'Docksal Solr Server',
//    'options' => array(
//      'host' => 'solr',
//      'port' => '8983',
//      'path' => '/solr/collection1',
//      'http_user' => '',
//      'http_pass' => '',
//      'excerpt' => 0,
//      'retrieve_data' => 0,
//      'highlight_data' => 0,
//      'http_method' => 'POST',
//    ),
//  ),
//);
